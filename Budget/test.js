    function initPage(param) {}

    function goto(dest) {
        var url = location.href.toLowerCase();
        var csr = url.lastIndexOf('/');
        var key = url.substr(csr - 3, 3);
        if (key == "eta" || key == "src" || key == "com") location.href = dest;
        else location.href = "../" + dest;
    }

    function checkForEnterKey(e) {
        var key = window.event ? e.keyCode : e.which;
        if (key == 13) doSearch();
    }

    function commentOnPage() {
        document.getElementById('refpage').value = document.title;
        document.getElementById('feedback').submit();
    }

    function emailPage() {
        document.getElementById('docTitle').value = document.title;
        document.getElementById('docUrl').value = myUrl();
        document.getElementById('emailto').submit();
    }

    function browserIsIE() {
        var browserName = navigator.appName;
        var res = (browserName == "Microsoft Internet Explorer");
        return res;
    }

    function isHosted() {
        var loc = location.href.toLowerCase();
        return (loc.substring(0, 4) == "http");
    }

    function myUrl() {
        return (window.location.href);
    }
    var dict = [];
    var searchRq = false;

    function doSearch() {
        var indexPage;
        var searchStr = document.getElementById("searchArg").value;
        if (searchStr.length == 0) return;
        var b = document.getElementById("searchbuffer");
        searchRq = true;
        if (location.href.indexOf("tocs") > 0) indexPage = "../indexpage.html";
        else indexPage = "indexpage.html";
        b.src = indexPage + "?" + searchStr;
    }

    function continueSearch() {
        if (!parent.searchRq) return;
        parent.searchRq = false;
        var srchStr = decodeURI(location.search.substr(1));
        var entries = document.body.innerHTML.toLowerCase();
        makeDict(entries);
        var keywordSet = srchStr.split(" ");
        for (var j = 0; j < keywordSet.length; j++) {
            result = validateKeyword(keywordSet[j]);
            if (result >= 0) {
                parent.location.href = "../" + dict[result] + ".html";
                return;
            }
        }
        parent.location.href = "notfound.html";
    }

    function validateKeyword(arg) {
        var keyword = arg.toLowerCase();
        var smap = mapOf(keyword);
        var dmap, closestWordIndex, bestScore = 999,
            thisWord;
        for (var wordIndex = 0; wordIndex < dict.length; wordIndex++) {
            thisWord = dict[wordIndex];
            if (thisWord == keyword) return wordIndex;
            else {
                dmap = mapOf(thisWord);
                errors = compare(dmap, smap);
                if (errors < bestScore) {
                    bestScore = errors;
                    closestWordIndex = wordIndex;
                }
            }
        }
        if (bestScore < 3) return closestWordIndex;
        else return -1;
    }

    function mapOf(s) {
        var c, map = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        for (var i = 0; i < s.length; i++) {
            c = s.charCodeAt(i);
            if (c >= 97 && c <= 122) map[c - 97]++;
        }
        return map;
    }

    function compare(dmap, smap) {
        var errors = 0;
        for (var i = 0; i < 26; i++) {
            errors += Math.abs(dmap[i] - smap[i]);
        }
        return errors;
    }

    function makeDict(entries) {
        var wordStart = 0,
            wordEnd, word;
        while (true) {
            wordStart = entries.indexOf("<h4>", wordStart + 1);
            if (wordStart < 0) break;
            wordEnd = entries.indexOf("</h4>", wordStart);
            word = entries.substring(wordStart + 4, wordEnd);
            dict.push(word);
        }
    }

    function reveal(obj) {
        document.getElementById(obj).style.visibility = 'visible';
    }

    function swap(me, obj) {
        me.style.display = "none";
        document.getElementById(obj).style.display = "inline";
    }

    function millisecTimeStamp() {
        var t = new Date();
        return t.getTime();
    }

    function showCalc() {
        window.open('calculator.html', 'mywindow', 'width=950,height=730,status=no,resizable=yes,scrollbars=yes')
    }
    var device = null;
    var appletLoadRq;
    var widthLoadRq;
    var heightLoadRq;
    var appletName;
    var appletContainerDiv = null;
    var htmlParams = "";
    var appletWid = 0;
    var appletHt = 0;
    var html5AppletIframe = null;
    var iFrameSrcFile = "";

    function loadJSApplet(applet, containerDiv, width, height, iFrameSRC) {
        appletName = applet;
        appletWid = width;
        appletHt = height;
        appletContainerDiv = document.getElementById(containerDiv);
        if (iFrameSRC == undefined) iFrameSrcFile = "appletframe";
        else iFrameSrcFile = iFrameSRC;
        loadActualApplet();
    }

    function loadActualApplet() {
        var ifrm = document.createElement('iframe');
        ifrm.setAttribute("name", "iframe");
        ifrm.setAttribute("frameBorder", "0");
        ifrm.setAttribute("width", "" + appletWid);
        ifrm.setAttribute("height", "" + appletHt);
        ifrm.setAttribute("scrolling", "no");
        ifrm.style.overflow = "hidden";
        ifrm.style.border = "1px solid #bbbbdd";
        appletContainerDiv.innerHTML = "";
        appletContainerDiv.appendChild(ifrm);
        html5AppletIframe = ifrm;
        var srcFileName = "common/" + iFrameSrcFile;
        if (location.href.indexOf("src") > 0) srcFileName += "debug";
        srcFileName += ".html";
        ifrm.src = srcFileName + "?applet=" + appletName + "&wid=" + appletWid + "&ht=" + appletHt;
    }
    var tgtAddress;
    var linkwindow;

    function makeLink(linkAddress) {
        tgtAddress = linkAddress;
        if (typeof (linkwindow) != "undefined") linkwindow.close();
        linkwindow = window.open('graphlinks.html', 'mywindow', 'width=600,height=450,status=yes,resizable=true,scrollbars=yes,menubar=yes')
    }

    function done() {
        if (typeof (linkwindow) != "undefined") linkwindow.close();
    }

    function isNumber(s) {
        return (s >= '0' && s <= '9') || s == '.';
    }

    function isVarnameChar(s) {
        var us = s.toLowerCase();
        return (us >= 'a' && us <= 'z') || s == '_' || isNumber(s);
    }

    function appendToBody(obj) {
        var bod = document.getElementsByTagName('body')[0];
        bod.appendChild(obj);
    }

    function getCurrentScript() {
        if (document.currentScript) {
            return document.currentScript;
        } else {
            var scripts = document.getElementsByTagName('script');
            return scripts[scripts.length - 1];
        }
    }

    function bookmarkPageIE() {}

    function bookmarkSiteIE() {}

    function Equation(param, classname, style, suppliedContainer, pScale) {
        Equation.prototype.makeContainer = function (classname, style) {
            var container = document.createElement("span");
            container.style.whiteSpace = "nowrap";
            if (classname) container.className = classname;
            if (style) container.style.cssText = style;
            var thisScript = getCurrentScript();
            return thisScript.parentNode.insertBefore(container, thisScript);
        };
        Equation.prototype.substitute = function substitute(str) {
            res = str;
            res = res.split("@sin").join("'\u202Fsin\u202F'");
            res = res.split("@cos").join("'\u202Fcos\u202F'");
            res = res.split("@tan").join("'\u202Ftan\u202F'");
            res = res.split("@csc").join("'\u202Fcsc\u202F'");
            res = res.split("@cot").join("'\u202Fcot\u202F'");
            res = res.split("@sec").join("'\u202Fsec\u202F'");
            res = res.split("@pi").join("\u03c0");
            res = res.split("@plusmin").join("\u00B1");
            res = res.split("@sqrt").join("\u221A");
            res = res.split("@times").join("\u00D7");
            res = res.split("-").join("\u2212");
            res = res.split("@dot").join("\u00B7");
            res = res.split("@deg").join("\u00B0");
            res = res.split("@sp").join("\u2008");
            res = res.split("@ang").join("\u2220");
            res = res.split("@sub").join("`");
            res = res.split("@le").join("\u202F\u2264\u202F");
            res = res.split("@ge").join("\u202F\u2265\u202F");
            res = res.split("@theta").join("\u03B8");
            res = res.split("@div").join("\u00F7");
            res = res.split("@inf").join("\u221E");
            return res;
        };
        Equation.prototype.tokenize = function (str) {
            var tokens = [];
            tokens.push('{');
            for (var i = 0; i < str.length; i++) {
                if (str[i] == "'") {
                    var strEnd = str.indexOf("'", i + 1);
                    if (strEnd < 0) strEnd = str.length;
                    tokens.push(str.substring(i + 1, strEnd));
                    i = strEnd;
                } else if (isNumber(str[i])) {
                    var val = "";
                    while (i < str.length && isNumber(str[i])) {
                        val += str[i++];
                    }
                    tokens.push(val);
                    i--;
                } else if (str[i] == "$") {
                    tokens.push(str[i++]);
                    var varname = "";
                    while (i < str.length && isVarnameChar(str[i])) {
                        varname += str[i++];
                    }
                    tokens.push(varname);
                    i--;
                } else if (str[i] != ' ') {
                    tokens.push(str[i]);
                }
            }
            tokens.push('}');
            return tokens;
        };
        Equation.prototype.makeParens = function () {
            var root = newNode();
            with(root.style) {
                display = "inline-table";
                verticalAlign = "middle";
                top = ".05em";
            }
            var item = this.vector.shift();
            var endCh = item == '(' ? ')' : '}';
            do {
                if (item != '}' && item != '{') {
                    var node = newNode(root);
                    node.innerHTML = item;
                    if (item == "=") node.style.padding = "0 0.3em";
                    if (item == "\u2212") node.style.padding = "0 0.06em";
                    if (item == "+") node.style.padding = "0 0.06em";
                }
                if (item == endCh || this.vector.length == 0) return root;
                if (this.vector[0] == '{' || this.vector[0] == '(') {
                    var elemNode = newNode(root);
                    var subTree = this.makeParens();
                    elemNode.appendChild(subTree);
                }
                item = this.vector.shift();
            } while (true);
        };
        Equation.prototype.fixHTML = function (rootNode, userFn, chr) {
            var divList = rootNode.getElementsByTagName("div");
            var tgtList = [];
            for (var i = 0; i < divList.length; i++) {
                var thisNode = divList[i];
                if (thisNode.firstChild && thisNode.firstChild.nodeValue == chr) {
                    tgtList.push(thisNode);
                }
            }
            for (var i = 0; i < tgtList.length; i++) {
                userFn.call(this, tgtList[i]);
            }
        };
        Equation.prototype.leftParenSwap = function (node) {
            if (node.parentNode.getElementsByClassName("frac").length > 0) {
                node.innerHTML = "&nbsp;";
                with(node.style) {
                    borderStyle = "solid";
                    borderTopLeftRadius = "100% 50%";
                    borderBottomLeftRadius = "100% 50%";
                    borderWidth = "0 0 0 " + this.lineWid + "px";
                    paddingLeft = "1px";
                }
                var spacer = document.createElement("div");
                node.parentNode.insertBefore(spacer, node);
                spacer.style.width = "0.1em";
            }
        };
        Equation.prototype.rightParenSwap = function (node) {
            if (node.parentNode.getElementsByClassName("frac").length > 0) {
                node.innerHTML = "&nbsp;";
                with(node.style) {
                    borderStyle = "solid";
                    borderTopRightRadius = "100% 50%";
                    borderBottomRightRadius = "100% 50%";
                    borderWidth = "0 " + this.lineWid + "px 0 0";
                    paddingLeft = "1px";
                }
                var spacer = document.createElement("div");
                node.parentNode.insertBefore(spacer, node.nextSibling);
                spacer.style.width = "0.1em";
            }
        };
        Equation.prototype.exponents = function (node) {
            var next = node.nextSibling;
            var prev = node.previousSibling;
            node.firstChild.nodeValue = "";
            var unit = newNode(node);
            with(unit.style) {
                display = "inline-table";
                paddingLeft = ".1em";
                paddingRight = ".1em";
            }
            unit.className = "exp";
            unit.appendChild(prev);
            unit.appendChild(next);
            with(next.style) {
                fontSize = "80%";
                verticalAlign = "top";
                top = "-1px";
                left = "1px";
            }
        };
        Equation.prototype.subscripts = function (node) {
            var next = node.nextSibling;
            var prev = node.previousSibling;
            node.firstChild.nodeValue = "";
            var unit = newNode(node);
            with(unit.style) {
                display = "inline-table";
                paddingLeft = ".3em";
                paddingRight = ".2em";
            }
            unit.className = "sub";
            unit.appendChild(prev);
            unit.appendChild(next);
            with(next.style) {
                fontSize = "80%";
                verticalAlign = "bottom";
                left = "1px";
            }
        };
        Equation.prototype.absolute = function (node) {
            node.firstChild.nodeValue = "";
            var prev = node.previousSibling;
            if (prev) prev.style.paddingRight = ".2em";
            var next = node.nextSibling;
            if (next) next.style.paddingLeft = ".2em";
            with(node.style) {
                borderLeft = ".1em solid ";
            }
        };
        Equation.prototype.radicals = function (node) {
            node.firstChild.nodeValue = "";
            var unit = newNode(node);
            unit.style.display = "inline-table";
            unit.style.verticalAlign = "middle";
            unit.className = "sqrt";
            var radical = newNode(unit);
            radical.innerHTML = "\u221A";
            with(radical.style) {
                paddingLeft = ".3em";
                radical.style.fontSize = "1.2em";
                radical.style.top = "0.2em";
            }
            unit.appendChild(radical);
            with(node.nextSibling.style) {
                borderTop = "1px solid";
            }
            unit.appendChild(node.nextSibling);
        };
        Equation.prototype.radicalSigns = function (node) {
            var radicand = node.nextSibling;
            if (radicand.getElementsByClassName("frac").length > 0) {
                node.innerHTML = "";
                node.style.width = "0.9em";
                var newNode = document.createElement("div");
                node.appendChild(newNode);
                newNode.innerHTML = "\u221A";
                with(newNode.style) {
                    fontSize = "200%";
                    transformOrigin = webkitTransformOrigin = "top right";
                    transform = webkitTransform = "scaleX(0.6)";
                    position = "absolute";
                    top = ".28em";
                    right = "0";
                }
            } else {
                radicand.style.paddingLeft = "0.2em";
            }
        };
        Equation.prototype.variables = function (node) {
            var next = node.nextSibling;
            varEntry = {};
            varEntry.name = next.innerHTML;
            varEntry.domNode = next;
            this.varTable.push(varEntry);
            node.parentNode.removeChild(node);
        };
        Equation.prototype.fractions = function (node) {
            node.firstChild.nodeValue = "";
            var unit = newNode(node);
            unit.style.display = "inline-table";
            unit.style.verticalAlign = "middle";
            unit.style.padding = "0 0.2em";
            unit.className = "frac";
            var prev = node.previousSibling;
            with(prev.style) {
                display = "block";
            }
            var next = node.nextSibling;
            with(next.style) {
                display = "block";
                borderTop = "1px solid";
            }
            unit.appendChild(prev);
            unit.appendChild(next);
        };
        Equation.prototype.update = function () {
            this.container.style.visibility = this.visible ? "visible" : "hidden";
            var decPlaces, plusSign, trailZeros;
            for (var i = 0; i < this.varTable.length; i++) {
                var elem = this.varTable[i].domNode;
                var varName = this.varTable[i].name;
                var value = window[varName];
                if (typeof value == 'object') {
                    var obj = value;
                    value = value.val;
                    elem.className = (typeof obj.classname == 'string') ? obj.classname : "vardefault";
                    decPlaces = (typeof obj.decPlaces == 'number') ? obj.decPlaces : 2;
                    plusSign = (typeof obj.plusSign == 'boolean') ? obj.plusSign : false;
                    trailZeros = (typeof obj.trailZeros == 'boolean') ? obj.trailZeros : true;
                    if (typeof value != "string") {
                        value = asString(value, decPlaces, plusSign, trailZeros);
                        if (plusSign) {
                            value = addSign(value);
                        }
                    }
                }
                elem.innerHTML = value;
            }
        };
        this.visible = true;
        this.varTable = [];
        this.scale = pScale ? pScale : 1;
        this.container = suppliedContainer ? suppliedContainer : this.makeContainer(classname, style);
        this.lineWid = Math.ceil(this.scale);
        if (parseInt(getComputedStyle(this.container, null).fontWeight) > 600) this.lineWid++;
        this.inputStr = this.substitute(param);
        this.vector = this.tokenize(this.inputStr);
        var divTree = this.makeParens();
        this.container.appendChild(divTree);
        this.fixHTML(divTree, this.variables, '$');
        this.fixHTML(divTree, this.exponents, '^');
        this.fixHTML(divTree, this.subscripts, '`');
        this.fixHTML(divTree, this.radicals, '\u221A');
        this.fixHTML(divTree, this.fractions, '/');
        this.fixHTML(divTree, this.absolute, '|');
        this.fixHTML(divTree, this.radicalSigns, '\u221A');
        this.fixHTML(divTree, this.leftParenSwap, '(');
        this.fixHTML(divTree, this.rightParenSwap, ')');
        this.update();

        function newNode(parent) {
            var elem = document.createElement("div");
            if (parent) parent.appendChild(elem);
            with(elem.style) {
                display = "table-cell";
                lineHeight = "1.1em";
                paddingBottom = "0.07em";
                verticalAlign = "baseline";
                textAlign = "center";
                position = "relative";
                boxSizing = "border-box";
                borderSpacing = "0";
            }
            return elem;
        };
    }

    function Formula(pEquation, pScale, pClassName) {
        this.container = document.createElement("div");
        this.container.className = pClassName;
        if (pScale > 1) {
            this.container.style.fontSize = Math.round(pScale * 100) + "%";
        }
        this.eqn = new Equation(pEquation, null, null, this.container, pScale);
        return this.eqn;
        this.update = function () {
            this.eqn.update();
        }
    }

    function showDom(root, level, input) {
        console.log(input);
        console.log(" ");
        showDom2(root, level);
    }

    function showDom2(root, level) {
        var sp = "";
        for (var i = 0; i < level * 3; i++) sp += " ";
        var out = sp;
        out += root.nodeName;
        if (root.nodeName == "#text") out += "    " + root.nodeValue;
        if (root.style) with(root.style) {
            if (display == "inline-table") out += "   unit";
            if (display == "table-cell") out += "   elem";
            out += "  " + root.className;
        }
        console.log(out);
        var node = root.firstChild;
        while (node) {
            showDom2(node, level + 1);
            node = node.nextSibling;
        }
    }
