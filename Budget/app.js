var BudgetController = (function () {
    var Expenses = (function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    });
    var Income = (function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    });
    var calculateTotal = (function (type) {
        var sum = 0;
        data.allItem[type].forEach(function (current) {
            sum += current.value;
        });
        data.totals[type] = sum;
    });
    var data = {
        allItem: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        parcentage: 0
    };
    return {
        addItem: function (type, description, value) {
            var newItem, id;
            if (data.allItem[type].length === 0) {
                id = 0;
            } else {
                id = data.allItem[type][data.allItem[type].length - 1] + 1;
            }
            if (type === 'exp') {
                newItem = new Expenses(id, description, value);
            } else {
                newItem = new Income(id, description, value);
            }
            data.allItem[type].push(newItem);
            return newItem;
        },
        calculateBudget: function() {
            calculateTotal('exp');
            calculateTotal('inc');
            data.budget = data.totals.inc - data.totals.exp;
            data.parcentage = Math.round((data.totals.exp / data.totals.inc) * 100);
        },
        getBudget: function(){
            return {
                totalBudget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                parcentage: data.parcentage
            }  
        },
        test: function() {
            console.log(data);
        }
    };
})();
var UIController = (function () {
    var DOMString = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list'
    };
    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMString.inputType).value,
                discription: document.querySelector(DOMString.inputDescription).value,
                value: parseFloat(document.querySelector(DOMString.inputValue).value)
            };
        },

        addListItem: function (obj, type) {
            var html, newHtml, element;
            // add HTML string
            if (type === 'inc') {
                element = DOMString.incomeContainer;
                html = '<div class="item clearfix" id="income-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div</div>';
            } else if (type === 'exp') {
                element = DOMString.expensesContainer;
                html = '<div class="item clearfix" id="expense-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div> <div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
            }
            // replace the placeholder text with actual data
            var x = '%id%';
            newHtml = html.replace(x, obj.id);

            newHtml = newHtml.replace('%description%', obj.description);

            newHtml = newHtml.replace('%value%', obj.value);

            // Insert htmal into the DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },
        clearFind: function () {
            var fild = document.querySelectorAll(DOMString.inputDescription + ', ' + DOMString.inputValue);
            var fildArray = Array.prototype.slice.call(fild);
            fildArray.forEach(function (current, i, array) {
                current.value = "";
            });
            fildArray[0].focus();
        },
        getDOMString: function () {
            return DOMString;
        }
    };
})();

var Contoller = (function (BudgetContr, UIContr) {
    var setupEventListener = function () {
        document.querySelector(DOM.inputBtn).addEventListener('click', ContAddItem);
        document.addEventListener('keypress', function (event) {
            if (event.keyCode == 13 || event.which == 13) {
                ContAddItem();
            }
        });
    };
    var DOM = UIContr.getDOMString();
    var UpdateBudget = (function () {
        // 1. calculate the budget
        BudgetContr.calculateBudget();
        // 2. Return the budget
        var budget = BudgetContr.getBudget();
        // 3. Display the budget to the UI
        console.log(budget);
    });
    var ContAddItem = (function () {
        var input, newItem;
        // 1. get fild input
        input = UIContr.getInput();

        if (input.discription !== "" && !isNaN(input.value) && input.value !== 0) {
            // 2. add iteam to the budget controller
            newItem = BudgetContr.addItem(input.type, input.discription, input.value);

            // 3. Add item to to the UI
            UIContr.addListItem(newItem, input.type);

            // 4. clear the fild
            UIContr.clearFind();

            UpdateBudget();
        }
    });

    return {
        init: function () {
            setupEventListener();
        }
    };

})(BudgetController, UIController);

Contoller.init();
