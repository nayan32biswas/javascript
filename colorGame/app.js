var colors, pickedColor, level = 3;
var squares = document.querySelectorAll(".square");
var displayColor = document.querySelector("#displayColor");
var message = document.querySelector("#message");
var resetButton = document.querySelector("#reset");
var easyBtn = document.querySelector("#easyBtn");
var hardBtn = document.querySelector("#hardBtn");

resetButton.addEventListener("click", reset);
document.querySelector("#easyBtn").addEventListener("click", function () {
    hardBtn.classList.remove("selected");
    easyBtn.classList.add("selected");
    level = 3;
    reset();
});
document.querySelector("#hardBtn").addEventListener("click", function () {
    easyBtn.classList.remove("selected");
    hardBtn.classList.add("selected");
    level = 6;
    reset();
});
reset();
for (var i = 0; i < squares.length; i++) {
    squares[i].style.background = colors[i];
    squares[i].addEventListener("click", function () {
        if (this.style.background === pickedColor) {
            Win();
        }
        else {
            this.style.background = "#232323";
            message.textContent = "Try Again";
        }
    });
}


function Win() {
    document.querySelector("h1").style.background = pickedColor;
    for (var i = 0; i < colors.length; i++) {
        squares[i].style.background = pickedColor;
    }
    message.textContent = "Correct";
    resetButton.textContent = "Play Again";
}
function reset() {
    colors = [];
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.background = "#232323";
        squares[i].style.display = "none";
    }
    colors = generateColor(level);
    document.querySelector("h1").style.background = "steelblue";
    for (var i = 0; i < colors.length; i++) {
        squares[i].style.background = colors[i];
        squares[i].style.display = "block";
    }
    message.textContent = "";
    resetButton.textContent = "New Game";
    pickedColor = colors[getRandom(0, colors.length)];
    displayColor.textContent = pickedColor;
}
function generateColor(num) {
    var array = [];
    for (var i = 0; i < num; i++) {
        array.push(makeColor());
    }
    return array;
}
function makeColor() {
    var r = ((getRandom(0, 256)).toString());
    var g = ((getRandom(0, 256)).toString());
    var b = ((getRandom(0, 256)).toString());
    return "rgb(" + r + ", " + g + ", " + b + ")";
}
function getRandom(lo, hi) {
    if (lo > hi) {
        [lo, hi] = [hi, lo];
    }
    var size = hi - lo;
    return lo + Math.floor(Math.random() * size);
}